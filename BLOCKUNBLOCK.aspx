﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user.master" AutoEventWireup="true" CodeFile="BLOCKUNBLOCK.aspx.cs" Inherits="BLOCKUNBLOCK" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" 
                            onselectedindexchanged="DataGrid2_SelectedIndexChanged" 
                            onitemcommand="DataGrid2_ItemCommand">
                            <Columns>
                                <asp:BoundColumn DataField="frommail" HeaderText="Name"></asp:BoundColumn>
                                <asp:ButtonColumn Text="Block"></asp:ButtonColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="style1">
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="Button2" runat="server" Text="VIEWBLOCKEDLIST" 
                                        onclick="Button2_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        <asp:DataGrid ID="DataGrid3" runat="server" AutoGenerateColumns="False" 
                            onselectedindexchanged="DataGrid3_SelectedIndexChanged" 
                            onitemcommand="DataGrid3_ItemCommand">
                            <Columns>
                                <asp:BoundColumn DataField="blockid" HeaderText="id"></asp:BoundColumn>
                                <asp:BoundColumn DataField="name" HeaderText="Name"></asp:BoundColumn>
                                <asp:BoundColumn DataField="touserid" HeaderText="Email id"></asp:BoundColumn>
                                <asp:ButtonColumn Text="UnBlock"></asp:ButtonColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

