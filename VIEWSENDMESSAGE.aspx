﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user.master" AutoEventWireup="true" CodeFile="VIEWSENDMESSAGE.aspx.cs" Inherits="VIEWSENDMESSAGE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" 
                            oneditcommand="DataGrid2_EditCommand" onitemcommand="DataGrid2_ItemCommand">
                            <Columns>
                                <asp:BoundColumn DataField="frommail" HeaderText="frommail" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tomail" HeaderText="tomail"></asp:BoundColumn>
                                <asp:BoundColumn DataField="snd_date" HeaderText="Date"></asp:BoundColumn>
                                <asp:BoundColumn DataField="message" HeaderText="msg" Visible="False">
                                </asp:BoundColumn>
                                <asp:ButtonColumn HeaderText="message" Text="message"></asp:ButtonColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <br />
            <table class="style1">
                <tr>
                    <td>
                        DATE</td>
                    <td style="margin-left: 40px">
                        <asp:TextBox ID="TextBox7" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        FROM</td>
                    <td style="margin-left: 40px">
                        <asp:TextBox ID="TextBox4" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        TO</td>
                    <td class="style2" style="margin-left: 40px">
                        <asp:TextBox ID="TextBox5" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        MESSAGE</td>
                    <td class="style2" style="margin-left: 40px">
                        <asp:TextBox ID="TextBox6" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
    <p>
    </p>
</asp:Content>

