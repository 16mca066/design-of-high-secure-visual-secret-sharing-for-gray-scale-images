﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user.master" AutoEventWireup="true" CodeFile="VIEWINBOX.aspx.cs" Inherits="VIEWINBOX" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 26px;
        }
        .style3
        {
            height: 55px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" 
                            onitemcommand="DataGrid2_ItemCommand" 
                            onselectedindexchanged="DataGrid2_SelectedIndexChanged">
                            <Columns>
                                <asp:BoundColumn DataField="id" HeaderText="id"></asp:BoundColumn>
                                <asp:BoundColumn DataField="frommail" HeaderText="frommail"></asp:BoundColumn>
                                <asp:BoundColumn DataField="tomail" HeaderText="tomail" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="message" HeaderText="message" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="snd_date" HeaderText="date" Visible="False">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="file_le" HeaderText="len" Visible="False">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="file_path" HeaderText="pth" Visible="False">
                                </asp:BoundColumn>
                                <asp:ButtonColumn Text="View" CommandName="View"></asp:ButtonColumn>
                                <asp:ButtonColumn CommandName="Star" Text="Star"></asp:ButtonColumn>
                                <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="style1">
                <tr>
                    <td class="style2">
                        DATE</td>
                    <td style="margin-left: 80px" class="style2">
                        <asp:TextBox ID="TextBox6" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        FROM</td>
                    <td style="margin-left: 80px">
                        <asp:TextBox ID="TextBox4" runat="server" ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        File</td>
                    <td style="margin-left: 80px">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td style="margin-left: 80px">
                        <asp:Button ID="Button1" runat="server" Height="29px" onclick="Button1_Click" 
                            Text="Get" Width="57px" />
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        MESSAGE</td>
                    <td class="style3">
                        <asp:TextBox ID="TextBox5" runat="server" ReadOnly="True" Height="42px" 
                            Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

