﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="viewuser.aspx.cs" Inherits="viewuser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 26px;
        }
        .style3
        {
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                onitemcommand="DataGrid1_ItemCommand" 
                onselectedindexchanged="DataGrid1_SelectedIndexChanged">
                <Columns>
                    <asp:BoundColumn DataField="user_id" HeaderText="regid"></asp:BoundColumn>
                    <asp:BoundColumn DataField="name" HeaderText="name"></asp:BoundColumn>
                    <asp:BoundColumn DataField="emailid" HeaderText="emailid"></asp:BoundColumn>
                    <asp:BoundColumn DataField="phone" HeaderText="phone">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="DOB" HeaderText="DOB" Visible="False">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="address" HeaderText="address"></asp:BoundColumn>
                    <asp:BoundColumn DataField="gender" HeaderText="gender" Visible="False"></asp:BoundColumn>
                    <asp:BoundColumn DataField="age" HeaderText="age">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="district" HeaderText="district" Visible="False">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="state" HeaderText="state">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="country" HeaderText="country" Visible="False">
                    </asp:BoundColumn>
                    <asp:ButtonColumn Text="View" Visible="False"></asp:ButtonColumn>
                </Columns>
            </asp:DataGrid>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table class="style1">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="mailid"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" ontextchanged="TextBox1_TextChanged"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="TextBox1" ErrorMessage="RequiredFieldValidator" 
                            ForeColor="Red">!!</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        name</td>
                    <td class="style2">
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="TextBox2" ErrorMessage="RequiredFieldValidator" 
                            ForeColor="Red">!!</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        address</td>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="TextBox3" ErrorMessage="RequiredFieldValidator" 
                            ForeColor="Red">!!</asp:RequiredFieldValidator>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                    </td>
                    <td class="style3">
                        <asp:Button ID="Button2" runat="server" Text="BLOCK" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

