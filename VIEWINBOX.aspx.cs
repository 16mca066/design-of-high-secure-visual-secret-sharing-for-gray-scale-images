﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class VIEWINBOX : System.Web.UI.Page
{
    dboperation db = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from sendmail where status='undelete' and tomail='" + Session["mail"] + "'";
        DataGrid2.DataSource = db.getdata(cmd);
        DataGrid2.DataBind();

        cmd.CommandText = "select max(id) from trash";
        id = db.max_id(cmd);
    }
    protected void DataGrid2_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            MultiView1.SetActiveView(View2);
            TextBox6.Text=e.Item.Cells[4].Text;
                TextBox4.Text=e.Item.Cells[1].Text;
            Label1.Text=e.Item.Cells[6].Text;
            Label2.Text=e.Item.Cells[5].Text;

        }
        else if (e.CommandName == "Star")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "update sendmail set status_star='star' where id='"+e.Item.Cells[0].Text+"'";
            db.execute(cmd);

            Response.Write("<script>alert('Stared')</script>");

        }
        else if (e.CommandName == "Delete")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "update sendmail set status='delete' where id='" + e.Item.Cells[0].Text + "'";
            db.execute(cmd);

            cmd.CommandText = "insert into trash values('" + id + "','" + e.Item.Cells[0].Text + "','" + e.Item.Cells[2].Text + "','" + e.Item.Cells[1].Text + "')";
            db.execute(cmd);

            Response.Write("<script>alert('Deleted')</script>");
            

            MultiView1.SetActiveView(View1);
            cmd.CommandText = "select * from sendmail where status='undelete' and tomail='" + Session["mail"] + "'";
            DataGrid2.DataSource = db.getdata(cmd);
            DataGrid2.DataBind();


        }
    }
    protected void DataGrid2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string path1 = Server.MapPath(Label1.Text);
        string path2 = FileUpload1.FileName;
        FileUpload1.SaveAs(Server.MapPath(path2));
        string ss = Server.MapPath(path2);
        VisualCrypto vc = new VisualCrypto();
        string res = vc.deCrypt(path1, Server.MapPath(path2), Convert.ToInt32(Label2.Text));

        TextBox5.Text = res;
        MultiView1.SetActiveView(View2);

        
    }
}