﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class sendmail : System.Web.UI.Page
{
    dboperation obj = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select max(id) from sendmail";
            id = obj.max_id(cmd);

            TextBox4.Text = Session["mail"].ToString();

            TextBox7.Text = System.DateTime.Now.ToShortDateString();

            cmd.CommandText = "select regid,emailid from usertable where emailid<>'" + Session["mail"] + "'";
            DropDownList1.DataSource = obj.getdata(cmd);
            DropDownList1.DataValueField = "regid";
            DropDownList1.DataTextField = "emailid";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, "Select");
        }

    }
    protected void TextBox4_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from blocktable where fromuserid='"+DropDownList1.SelectedItem+"'";
        DataTable dt = obj.getdata(cmd);
        if (dt.Rows.Count > 0)
        {
            Response.Write("<script>alert('This user is your blocked')</script>");

        }
        else
        {
            string ipath = "~/temp/" + FileUpload1.FileName;
            FileUpload1.SaveAs(Server.MapPath(ipath));
            string pth1 = Server.MapPath(ipath);
            string msg = TextBox6.Text;
            string opath1 = Server.MapPath("~/mail/" + id + "_1.jpg");//first share path
            string opath2 = Server.MapPath("~/temp/" + id + "_2.jpg");//second share path
            string pth3="~/mail/" + id + "_1.jpg";
            VisualCrypto vc = new VisualCrypto();
            vc.enCrypt(Server.MapPath(ipath), opath1, opath2, msg, msg.Length);



            cmd.CommandText = "insert into sendmail values('" + id + "','" + TextBox4.Text + "','" + DropDownList1.SelectedItem + "','" + TextBox6.Text + "','" + TextBox7.Text + "','unstar','undelete','unblock','"+pth3+"','"+msg.Length+"')";
            obj.execute(cmd);


            string filename = opath2;  //Filelocation
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename);
            Response.WriteFile(filename);
            Response.End();

            TextBox4.Text = "";
            TextBox7.Text = "";
            TextBox6.Text = "";

            DropDownList1.SelectedIndex = -1;

            cmd.CommandText = "select max(id) from sendmail";
            id = obj.max_id(cmd);

            TextBox4.Text = Session["mail"].ToString();

        }
    }
    protected void TextBox7_TextChanged(object sender, EventArgs e)
    {

    }
}