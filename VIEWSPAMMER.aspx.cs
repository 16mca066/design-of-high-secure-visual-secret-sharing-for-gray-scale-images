﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class VIEWSPAMMER : System.Web.UI.Page
{
    dboperation db = new dboperation();
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT sparm.sparmid, sparm.date, sparm.spam, usertable.name FROM sparm INNER JOIN usertable ON sparm.user_id = usertable.regid";
        DataGrid2.DataSource = db.getdata(cmd);
        DataGrid2.DataBind();
    }
}