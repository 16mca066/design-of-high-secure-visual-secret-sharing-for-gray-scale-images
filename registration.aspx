﻿<%@ Page Title="" Language="C#" MasterPageFile="~/login.master" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            height: 26px;
        }
        .style3
        {
            height: 30px;
        }
        .style4
        {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <table class="style1">
            <tr>
                <td class="style2">
                    NAME</td>
                <td class="style2">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="TextBox4" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="#CC0000">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    ADDRESS</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" ontextchanged="TextBox5_TextChanged" 
                        style="height: 22px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="TextBox5" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    DOB</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                        ControlToValidate="TextBox6" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="TextBox6" ErrorMessage="Invalid Date" ForeColor="Red" 
                        ValidationExpression="(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    GENDER</td>
                <td>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                        RepeatDirection="Horizontal">
                        <asp:ListItem>male</asp:ListItem>
                        <asp:ListItem>female</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                        ControlToValidate="RadioButtonList1" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    AGE</td>
                <td style="margin-left: 40px" class="style3">
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="TextBox7" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="#CC0000">!!</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                        ControlToValidate="TextBox7" ErrorMessage="RegularExpressionValidator" 
                        ForeColor="Red" ValidationExpression="[0-9]{1,3}">Invalid Age</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    DISTRICT</td>
                <td style="margin-left: 80px" class="style3">
                    <asp:TextBox ID="TextBox8" runat="server" style="height: 22px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="TextBox8" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    STATE</td>
                <td style="margin-left: 80px">
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                        ControlToValidate="TextBox9" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    COUNTRY</td>
                <td style="margin-left: 80px" class="style4">
                    <asp:TextBox ID="TextBox10" runat="server" 
                        ontextchanged="TextBox14_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                        ControlToValidate="TextBox10" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="#FF3300">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    MOBILE NO</td>
                <td style="margin-left: 80px" class="style3">
                    <asp:TextBox ID="TextBox11" runat="server" 
                        ontextchanged="TextBox15_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                        ControlToValidate="TextBox11" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                        ControlToValidate="TextBox11" ErrorMessage="invalid mobile number" 
                        ForeColor="#CC0000" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    EMAIL</td>
                <td style="margin-left: 80px">
                    <asp:TextBox ID="TextBox12" runat="server" style="margin-top: 0px" 
                        ontextchanged="TextBox12_TextChanged"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                        ControlToValidate="TextBox12" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="#CC0000">!!</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                        ControlToValidate="TextBox12" ErrorMessage="invalid email" ForeColor="#CC0000" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    PASSWORD</td>
                <td style="margin-left: 80px">
                    <asp:TextBox ID="TextBox13" runat="server" style="margin-top: 0px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                        ControlToValidate="TextBox13" ErrorMessage="RequiredFieldValidator" 
                        ForeColor="Red">!!</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style3">
                </td>
                <td class="style3" style="margin-left: 80px">
                    <asp:Button ID="Button2" runat="server" Text="FINISH" onclick="Button2_Click" />
                </td>
            </tr>
        </table>
    </p>
</asp:Content>

