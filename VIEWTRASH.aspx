﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user.master" AutoEventWireup="true" CodeFile="VIEWTRASH.aspx.cs" Inherits="VIEWTRASH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" 
                    onitemcommand="DataGrid2_ItemCommand">
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="id"></asp:BoundColumn>
                        <asp:BoundColumn DataField="msg_id" HeaderText="msg_id" Visible="False">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="message" HeaderText="Message"></asp:BoundColumn>
                        <asp:BoundColumn DataField="t_mailid" HeaderText="Mail Id"></asp:BoundColumn>
                        <asp:ButtonColumn Text="Restore"></asp:ButtonColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

