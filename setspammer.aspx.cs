﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class setspammer : System.Web.UI.Page
{
    dboperation obj = new dboperation();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT        sparm.sparmid, sparm.date, sparm.user_id, usertable.emailid, sparm.spam FROM sparm INNER JOIN usertable ON sparm.user_id = usertable.regid";
            DataGrid1.DataSource = obj.getdata(cmd);
            DataGrid1.DataBind();

            cmd.CommandText = "select regid,emailid from usertable";
            DropDownList1.DataSource = obj.getdata(cmd);
            DropDownList1.DataValueField = "regid";
            DropDownList1.DataTextField = "emailid";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, "Select");


            cmd.CommandText = "select max(sparmid) from sparm";
            id = obj.max_id(cmd);

            TextBox3.Text = System.DateTime.Now.ToShortDateString();
        }
    }
    protected void DataGrid1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "delete from sparm where sparmid ='" + e.Item.Cells[0].Text + "'";
        obj.execute(cmd);

        cmd.CommandText = "SELECT        sparm.sparmid, sparm.date, sparm.user_id, usertable.emailid, sparm.spam FROM sparm INNER JOIN usertable ON sparm.user_id = usertable.regid";
        DataGrid1.DataSource = obj.getdata(cmd);
        DataGrid1.DataBind();

        MultiView1.SetActiveView(View1);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View2);
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "insert into sparm values('" + id + "','" + TextBox3.Text+ "','" + DropDownList1.SelectedValue + "','" + TextBox2.Text + "')";
        obj.execute(cmd);

        Response.Write("<script>alert('success')</script>");

        cmd.CommandText = "select max(sparmid) from sparm";
        id = obj.max_id(cmd);

        cmd.CommandText = "SELECT        sparm.sparmid, sparm.date, sparm.user_id, usertable.emailid, sparm.spam FROM sparm INNER JOIN usertable ON sparm.user_id = usertable.regid";
        DataGrid1.DataSource = obj.getdata(cmd);
        DataGrid1.DataBind();

        MultiView1.SetActiveView(View1);
        TextBox2.Text = "";
    }
}